#rajesh m
import re

# -1 for phishing or 1 for legitimate

# Address Bar Features: Having_ip_address in URL

def category1(url):
    #1 IP address in URL
    match = re.search('[0-9]{1,3}[.]+?[0-9]{1,3}[.]+?[0-9]{0,3}[.]+?[0-9]{1,3}',url)
    file_obj = open(r'phishing5.txt','w')
    if match!=None:
        file_obj.write('-1,')
    else:
        file_obj.write('1,')
    #2.URL_length
    if len(url)<54:
        file_obj.write('1,')
    elif len(url)<=75 and len(url)>=54:
        file_obj.write('0,')
    else:
        file_obj.write('-1,')
    #3. Tiny_url - This code should be fixed!!!!
    if len(url)<22:
        file_obj.write('-1,')
    else:
        file_obj.write('1,')
    #4. "@"_found in URL
    at_match = re.search('[@]+?',url)
    if at_match != None:
        file_obj.write('-1,')
    else:
        file_obj.write('1,')
    #5. "//" found_in URL
    dble_slash_match = re.findall('//',url)
    if len(dble_slash_match)>1:
        file_obj.write('-1,')
    else:
        file_obj.write('1,')
    #6. "-" found in URL
    dash_match = re.search('-',url)
    if dash_match:
        file_obj.write('-1,')
    else:
        file_obj.write('1,')
    #7. "https"_ token found in URL
    https_match = re.search('https-',url)
    if https_match:
        file_obj.write('-1,')
    else:
        file_obj.write('1,')

    file_obj.close()
