#rajesh m
import p1
import p2
import p3
import p4
import pandas as pd

websites = ["Jeff-Bezos.info","gestaocaxa-com-br.umbler.net", "kiwanis.org", "web99841040km-com.preview-domain.com/1081110121.htm", "google.com","pestcontrol10.com/directing/www1.royalbank.com/cgi-bin/rbaccess/rbunxcgi/ClientSignin.htm"]
print("*******************************************************************")
print("API is listening...")
print("New email arrived...")
print("Scanning URLs in raw email message...")
print("*******************************************************************")
for website in websites:

    print("Request:  {\"url\":\"" + website + "\"}")
    p1.category1(website)
    p2.category2(website)
    p3.category3(website)
    p4.category4(website)

    read = pd.read_csv(r'phishing5.txt', header=None, sep=',')
    read = read.iloc[:, :-1].values
    dataset = pd.read_csv(r'Training Dataset1.csv')
    X = dataset.iloc[:, :-1].values
    y = dataset.iloc[:, -1].values

    from sklearn.model_selection import train_test_split

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1001)

    from sklearn.ensemble import RandomForestRegressor

    regressor = RandomForestRegressor(n_estimators=10, criterion="mse", random_state=2)
    regressor.fit(X_train, y_train)

    y_pred = regressor.predict(X_test)

    from sklearn.model_selection import cross_val_score

    accuracy = cross_val_score(estimator=regressor, X=X_train, y=y_train, cv=5)
    accuracy.mean()
    accuracy.std()

    Detect_phishing_website = regressor.predict(read)

    if Detect_phishing_website:
        print("Response: {\"result\": %1d, \"accuracy\": %0.2f, \"std\": %0.2f }" % (
        Detect_phishing_website, accuracy.mean(), accuracy.std() * 2))
    else:
        print("Response: {\"result\": -99, \"accuracy\" : 0.00%, \"std\" : 0.00 }")
    print("*******************************************************************")
